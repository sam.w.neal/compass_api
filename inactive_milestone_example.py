# This is an example of how to pass this API a csv of usernames and have it
# loop through and add an inactive milestone to each one.
import api
import csv
import time

driver_path = "C:\\Program Files (x86)\\chromedriver\\chromedriver.exe"
compass_url = "https://mckinnonsc-vic.compass.education/Organise/PeopleManagement/"
compass = api.CompassApi(executable_path=driver_path, compass_url=compass_url)
compass.sign_in()

with open('users.csv', 'r') as f:
    reader = csv.reader(f)
    users = list(reader)

users = [x[0] for x in users]

for user in users:
    compass.open_people(user)
    time.sleep(1)
    compass.set_import_mode('Locked')
    compass.add_milestone(is_active=False)
    compass.save_people()
    compass.sign_in()
    time.sleep(5)
