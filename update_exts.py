import api
import time
import pandas as pd

# Setup
driver_path = "C:\Program Files (x86)\Chrome Driver\chromedriver.exe"
compass_url = "https://mckinnonsc-vic.compass.education/Configure/UserExtensionManagement.aspx"
compass = api.CompassApi(executable_path=driver_path, compass_url=compass_url)
compass.sign_in()

'''
 1. Remove any blank extensions from staff_list.csv, this can't handle null values
 2. Make sure all usernames are lowercase
'''
exts = pd.read_csv('staff_list.csv', index_col=0, squeeze=True).to_dict()
table = compass.get_compass_table('ext-comp-1009-body')
i=1
for row in table:
    user = row.text.split()[0].lower()
    try:
        compass.update_extension(row, exts[user])
        print(i)
        i = i+1
    except KeyError:
        print(f"Can't find: {user}")
        pass
