from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import WebDriverException, TimeoutException, NoSuchElementException
import time


class CompassApi(webdriver.Chrome):
    def __init__(self, compass_url=None, *args, **kwargs):
        super(CompassApi, self).__init__(*args, **kwargs)
        self.compass_url = compass_url

    def sign_in(self, url=None, timeout=30):
        url = self.compass_url or url
        self.get(url)
        '''
        Wait for login. When 'password' element is no longer visible, 
        assume login successful
        '''
        print('Please sign into Compass now...')
        try:
            WebDriverWait(self, timeout).until(EC.invisibility_of_element_located(
                    self.find_element_by_id("password")
                )) 
        except (TimeoutException, NoSuchElementException):
            print(f"Login timeout exceeded ({timeout}s)")
            pass


    def get_compass_table(self, table_id):
        # Can't get a real WebDriverWait to work 😔
        time.sleep(5)
        table = self.find_element_by_id(table_id)
        return table.find_elements_by_tag_name('tr')

    def get_extensions(self, compass_table):
        '''
        Takes a compass_table and returns a dict eg. {'samn': '1234'}
        Doesn't work yet with users who have multiple first or last names
        '''
        _ = {}
        for row in compass_table:
            try:
                _[row.text.split()[0]] = row.text.split()[3]
            except IndexError:
                pass
        return _

    def update_extension(self, compass_row, extension):
        try:
            compass_row.find_elements_by_tag_name('td')[-1].click()
            self.find_element_by_id('textfield-1021-inputEl').send_keys(extension)
        except Exception:
            print(f"FAILED: {extension}")
            pass


    # Opens the user properties from the People Management module
    def open_people(self, username):
        pm_search_id = 'textfield-1136-inputEl'

        self.filter_table(pm_search_id, username)
        time.sleep(2)
        table = self.get_compass_table('pagedgrid-1033-body')

        user_element = [row for row in table
                        if row.find_elements_by_tag_name('td')[1].text.lower() == username.lower()][0]

        ActionChains(self).double_click(user_element).perform()

    # Fills out the search / filter text box on a Compass Table
    def filter_table(self, search_id, filter_text):
        filter_element = self.find_element_by_id(search_id)
        filter_element.clear()
        filter_element.send_keys(filter_text)

    def open_people_tab(self, name):
        [x for x in self.find_elements_by_css_selector('[id*="tab-"]') if
         x.text == name][0].click()

    def add_milestone(self, is_active, start_date=time.strftime('%d/%m/%Y')):
        self.open_people_tab('School')
        [x for x in self.find_elements_by_css_selector('span[id*="button-"]') if
         x.text == 'Add New Milestone'][0].click()
        self.find_element_by_name('startExt').send_keys(start_date)
        # THIS IS NASTY
        if not is_active:
            for x in self.find_elements_by_css_selector('input[id*="checkbox-"]'):
                try:
                    x.click()
                except WebDriverException:
                    None

        [x for x in self.find_elements_by_css_selector('span[id*="button-"]') if
         x.text == 'Add Milestone'][0].click()

    # get_import_mode().get_property('value') to get string value
    def get_import_mode(self):
        self.open_people_tab('Compass')
        return [x for x in
                self.find_elements_by_css_selector('input')
                if x.get_property('value') in self.valid_import_modes][0] or None

    # Takes a get_import_mode() and changes it to import_mode
    def set_import_mode(self, import_mode):
        self.open_people_tab('Compass')
        import_mode_ele = self.get_import_mode()
        if import_mode_ele.get_property('value') != 'Locked':
            print('yee this is running...')
            import_mode_ele.click()
            time.sleep(1)
            [mode.click() for mode in
             self.find_elements_by_class_name('x-boundlist-item')
             if mode.text == import_mode]

    def save_people(self):
        [tag for tag in self.find_elements_by_tag_name('span')
         if tag.text == 'Save'][0].click()
